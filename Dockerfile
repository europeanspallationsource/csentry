FROM continuumio/miniconda3:latest

RUN groupadd -r ics && useradd -r -g ics ics

WORKDIR /app

# Install CSEntry requirements
COPY environment.yml /app/environment.yml
RUN conda config --add channels conda-forge \
    && conda env create -n csentry -f environment.yml \
    && rm -rf /opt/conda/pkgs/*

# Install the app
COPY . /app/
RUN chown -R ics:ics /app/*

# activate the csentry environment
ENV PATH /opt/conda/envs/csentry/bin:$PATH
