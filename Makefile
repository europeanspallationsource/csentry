.PHONY: help build tag push refresh release db initdb test db_image test_image

OWNER := registry.esss.lu.se/ics-infrastructure
GIT_TAG := $(shell git describe --always)
IMAGE := csentry


help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "CSEntry"
	@echo "======="
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the latest image
	docker build -t $(OWNER)/$(IMAGE):latest .

tag: ## tag the latest image with the git tag
	docker tag $(OWNER)/$(IMAGE):latest $(OWNER)/$(IMAGE):$(GIT_TAG)

push: ## push the latest and git tag image
	docker push $(OWNER)/$(IMAGE):$(GIT_TAG)
	docker push $(OWNER)/$(IMAGE):latest

clean: ## remove the image with git tag and the test database
	-docker rmi $(OWNER)/$(IMAGE):$(GIT_TAG)
	-docker rm -f csentry_postgres
	-docker rm -f csentry_redis

refresh: ## pull the latest image from Docker Hub
# skip if error: image might not be on dockerhub yet
	-docker pull $(OWNER)/$(IMAGE):latest

release: refresh \
	build \
	tag \
	push
release: ## build, tag, and push all stacks

db: ## start postgres and redis for development
	docker-compose up -d postgres redis

initdb: ## initialize the dev database
	docker-compose run --rm web flask initdb

test:  ## run the tests (on current directory)
	docker-compose run --rm web pytest --cov=app -v

db_image: ## start postgres and redis to test the latest image
# Pass docker-compose.yml to skip docker-compose.override.yml
	docker-compose -f docker-compose.yml up -d postgres redis

test_image:  ## run the tests (on the latest image)
# Pass docker-compose.yml to skip docker-compose.override.yml
	docker-compose -f docker-compose.yml run --rm web

run_uwsgi:  ## run the application with uwsgi (to test prod env)
	docker-compose run -p 8000:8000 --rm web uwsgi --master --http 0.0.0.0:8000 --http-keepalive --manage-script-name --mount /csentry=wsgi.py --callable app --uid ics --processes 2 -b 16384
