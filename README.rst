CSEntry
=======

**WARNING**
This repository is deprecated. Official repository moved to GitLab: https://gitlab.esss.lu.se/ics-infrastructure/csentry

Control System Entry web server.


Development
-----------

You can use docker for development:

1. Clone the repository

2. Create the database. Data will be stored under "./data" by default.
   You can export the PGDATA_VOLUME variable to use another directory::

    # Start only postgres so it has time to initialize
    $ docker-compose up -d postgres
    or
    $ make db
    # Initialize the database
    $ docker-compose run --rm web flask initdb
    or
    $ make initdb

3. Start the application::

    $ docker-compose up

4. Open your browser and go to `http://localhost:8000
   <http://localhost:8000>`_.

5. To run the tests::

    $ make test


Once the database has been created, you only need to run `docker-compose
up` to start the app.


Testing
-------

By default docker-compose reads both docker-compose.yml and docker-compose.override.yml.
In docker-compose.override.yml, the current directory is mounted as a volume.
This is great for development.

To test the built image, we should only use the docker-compose.yml (and ignore
docker-compose.override.yml).

1. Create the database::

    # Start only postgres so it has time to initialize
    $ docker-compose -f docker-compose.yml up -d postgres
    or
    $ make db_image

2. Run the tests::

    $ docker-compose -f docker-compose.yml run --rm web
    or
    $ make test_image


Backup & restore
----------------

To dump the database::

    $ docker run --rm --link csentry_postgres:postgres --net csentry_default -e PGPASSWORD="<csentry_password>"
      postgres:10 pg_dump -h postgres -U csentry csentry_db | gzip > csentry_db.dump.gz


To restore the database::

    $ gunzip -c csentry_db.dump.g | docker run --rm --link csentry_postgres:postgres --net csentry_default
      -e PGPASSWORD="<csentry_password>" -i postgres:10 psql -h postgres -U csentry csentry_db


Dependencies
------------

The initial dependencies were generated using::

    $ docker run --rm -it -v $(pwd):/app continuumio/miniconda3:latest bash
    $ conda config --add channels conda-forge
    $ conda create -n csentry python=3.6 flask alembic flask-debugtoolbar flask-login flask-sqlalchemy flask-wtf pillow psycopg2 pytest pytest-cov qrcode whitenoise factory_boy flask-admin pyjwt ldap3 flask-mail flask-migrate flask-jwt-extended
    $ source activate csentry
    $ pip install flask-ldap3-login sqlalchemy-citext sqlalchemy-continuum pytest-factoryboy git+https://github.com/beenje/flask-bootstrap@4.0.0-beta.1.dev1
    $ conda env export > /app/environment.yml
