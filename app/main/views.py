# -*- coding: utf-8 -*-
"""
app.main.views
~~~~~~~~~~~~~~

This module implements the main blueprint.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint, render_template, jsonify
from flask_login import login_required
from .. import utils

bp = Blueprint('main', __name__)


# Declare custom error handlers for all views
@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@bp.app_errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


@bp.app_errorhandler(utils.CSEntryError)
def handle_csentry_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@bp.route('/')
@login_required
def index():
    return render_template('index.html')
