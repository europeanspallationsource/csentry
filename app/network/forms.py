# -*- coding: utf-8 -*-
"""
app.network.forms
~~~~~~~~~~~~~~~~~

This module defines the network blueprint forms.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask_login import current_user
from wtforms import (SelectField, StringField, TextAreaField, IntegerField,
                     SelectMultipleField, BooleanField, validators)
from ..helpers import CSEntryForm
from ..validators import (Unique, RegexpList, IPNetwork, HOST_NAME_RE,
                          VLAN_NAME_RE, MAC_ADDRESS_RE)
from .. import utils, models


def validate_tags(form, field):
    choices = dict(field.choices)
    for tag_id in field.data:
        tag_name = choices.get(tag_id)
        if tag_name == 'gateway':
            network = models.Network.query.get(form.network_id.data)
            existing_gateway = network.gateway()
            if existing_gateway is not None:
                raise validators.ValidationError(f'A gateway is already defined for network {network}: {existing_gateway}')


class NoValidateSelectField(SelectField):
    """SelectField with no choices validation

    By default a SelectField tries to validate the selected value
    against the list of choices. This is not possible when the choices
    are dynamically created on the browser side.
    """

    def pre_validate(self, form):
        pass


class NetworkScopeForm(CSEntryForm):
    name = StringField('Name',
                       description='name must be 3-25 characters long and contain only letters, numbers and dash',
                       validators=[validators.InputRequired(),
                                   validators.Regexp(VLAN_NAME_RE),
                                   Unique(models.NetworkScope, column='name')])
    description = TextAreaField('Description')
    first_vlan = IntegerField('First vlan')
    last_vlan = IntegerField('Last vlan')
    supernet = StringField('Supernet',
                           validators=[validators.InputRequired(),
                                       IPNetwork()])


class NetworkForm(CSEntryForm):
    scope_id = SelectField('Network Scope')
    vlan_name = StringField('Vlan name',
                            description='vlan name must be 3-25 characters long and contain only letters, numbers and dash',
                            validators=[validators.InputRequired(),
                                        validators.Regexp(VLAN_NAME_RE),
                                        Unique(models.Network, column='vlan_name')])
    vlan_id = NoValidateSelectField('Vlan id', choices=[])
    description = TextAreaField('Description')
    prefix = NoValidateSelectField('Prefix', choices=[])
    address = NoValidateSelectField('Address', choices=[])
    first_ip = NoValidateSelectField('First IP', choices=[])
    last_ip = NoValidateSelectField('Last IP', choices=[])
    admin_only = BooleanField('Admin only')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope_id.choices = utils.get_model_choices(models.NetworkScope, attr='name')


class HostForm(CSEntryForm):
    name = StringField('Hostname',
                       description='hostname must be 2-20 characters long and contain only letters, numbers and dash',
                       validators=[validators.InputRequired(),
                                   validators.Regexp(HOST_NAME_RE),
                                   Unique(models.Host)],
                       filters=[utils.lowercase_field])
    type = SelectField('Type', choices=utils.get_choices(('Virtual', 'Physical')))
    description = TextAreaField('Description')
    item_id = SelectField('Item', coerce=utils.coerce_to_str_or_none)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.item_id.choices = utils.get_model_choices(models.Item, allow_none=True, attr='ics_id')


class InterfaceForm(CSEntryForm):
    host_id = SelectField('Host')
    network_id = SelectField('Network')
    # The list of IPs is dynamically created on the browser side
    # depending on the selected network
    ip = NoValidateSelectField('IP', choices=[])
    interface_name = StringField(
        'Interface name',
        description='name must be 2-20 characters long and contain only letters, numbers and dash',
        validators=[validators.InputRequired(),
                    validators.Regexp(HOST_NAME_RE),
                    Unique(models.Interface)],
        filters=[utils.lowercase_field])
    mac_address = StringField(
        'MAC',
        validators=[validators.Optional(),
                    validators.Regexp(MAC_ADDRESS_RE, message='Invalid MAC address')])
    cnames_string = StringField(
        'Cnames',
        description='space separated list of cnames (must be 2-20 characters long and contain only letters, numbers and dash)',
        validators=[validators.Optional(),
                    RegexpList(HOST_NAME_RE)])
    tags = SelectMultipleField('Tags', coerce=utils.coerce_to_str_or_none,
                               validators=[validate_tags])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host_id.choices = utils.get_model_choices(models.Host)
        if current_user.is_admin:
            network_query = models.Network.query
            tags_query = models.Tag.query
        else:
            network_query = models.Network.query.filter(models.Network.admin_only.is_(False))
            tags_query = models.Tag.query.filter(models.Tag.admin_only.is_(False))
        self.network_id.choices = utils.get_model_choices(models.Network, allow_none=False,
                                                          attr='vlan_name', query=network_query)
        self.tags.choices = utils.get_model_choices(models.Tag, allow_none=True,
                                                    attr='name', query=tags_query)


class HostInterfaceForm(HostForm, InterfaceForm):
    pass
