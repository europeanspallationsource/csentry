# -*- coding: utf-8 -*-
"""
app.defaults
~~~~~~~~~~~~

This module implements the database default values.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from . import models


defaults = [
    models.Action(name='Assign ICS id'),
    models.Action(name='Clear all'),
    models.Action(name='Clear attributes'),
    models.Action(name='Fetch'),
    models.Action(name='Register'),
    models.Action(name='Set as parent'),
    models.Action(name='Update'),

    models.Tag(name='gateway', admin_only=True),
]
