# -*- coding: utf-8 -*-
"""
app.inventory.forms
~~~~~~~~~~~~~~~~~~~

This module defines the inventory blueprint forms.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from wtforms import SelectField, StringField, IntegerField, TextAreaField, validators
from ..helpers import CSEntryForm
from ..validators import Unique, RegexpList, ICS_ID_RE, MAC_ADDRESS_RE
from .. import utils, models


class AttributeForm(CSEntryForm):
    name = StringField('Name', validators=[validators.DataRequired()])
    description = StringField('Description')


class ItemForm(CSEntryForm):
    ics_id = StringField('ICS id',
                         validators=[validators.InputRequired(),
                                     validators.Regexp(ICS_ID_RE),
                                     Unique(models.Item, 'ics_id')])
    serial_number = StringField('Serial number',
                                validators=[validators.InputRequired()])
    quantity = IntegerField('Quantity', default=1,
                            validators=[validators.NumberRange(min=1)])
    manufacturer_id = SelectField('Manufacturer', coerce=utils.coerce_to_str_or_none)
    model_id = SelectField('Model', coerce=utils.coerce_to_str_or_none)
    location_id = SelectField('Location', coerce=utils.coerce_to_str_or_none)
    status_id = SelectField('Status', coerce=utils.coerce_to_str_or_none)
    parent_id = SelectField('Parent', coerce=utils.coerce_to_str_or_none)
    mac_addresses = StringField(
        'MAC addresses',
        description='space separated list of MAC addresses',
        validators=[validators.Optional(),
                    RegexpList(MAC_ADDRESS_RE, message='Invalid MAC address')])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.manufacturer_id.choices = utils.get_model_choices(models.Manufacturer, allow_none=True)
        self.model_id.choices = utils.get_model_choices(models.Model, allow_none=True)
        self.location_id.choices = utils.get_model_choices(models.Location, allow_none=True)
        self.status_id.choices = utils.get_model_choices(models.Status, allow_none=True)
        self.parent_id.choices = utils.get_model_choices(models.Item, allow_none=True, attr='ics_id')


class CommentForm(CSEntryForm):
    body = TextAreaField('Enter your comment:',
                         validators=[validators.DataRequired()])
