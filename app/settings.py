# -*- coding: utf-8 -*-
"""
app.settings
~~~~~~~~~~~~

This module implements the app default settings.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import os
from datetime import timedelta

SQLALCHEMY_DATABASE_URI = 'postgresql://ics:icspwd@postgres/csentry_db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
BOOTSTRAP_SERVE_LOCAL = True
SECRET_KEY = (os.environ.get('SECRET_KEY') or
              b'\x0d\x11{\xd3\x13$\xeeel\xa6\xfb\x1d~\xfd\xb2\x9d\x16\x00\xfb5\xd64\xd4\xe0')
MAIL_SERVER = 'mail.esss.lu.se'
MAIL_CREDENTIALS = None
ADMIN_EMAILS = ['admin@example.com']
EMAIL_SENDER = 'noreply@esss.se'

JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=12)

SESSION_TYPE = 'redis'
REDIS_URL = 'redis://redis:6379/0'
CACHE_TYPE = 'redis'
CACHE_REDIS_URL = 'redis://redis:6379/1'

LDAP_HOST = 'esss.lu.se'
LDAP_BASE_DN = 'DC=esss,DC=lu,DC=se'
LDAP_USER_DN = 'OU=ESS Users'
LDAP_GROUP_DN = ''
LDAP_BIND_USER_DN = 'ldapuser'
LDAP_BIND_USER_PASSWORD = 'secret'
LDAP_USER_RDN_ATTR = 'cn'
LDAP_USER_LOGIN_ATTR = 'sAMAccountName'
LDAP_ALWAYS_SEARCH_BIND = True
LDAP_USER_OBJECT_FILTER = '(samAccountType=805306368)'
LDAP_GROUP_OBJECT_FILTER = ''
LDAP_USER_SEARCH_SCOPE = 'SUBTREE'
LDAP_GROUP_SEARCH_SCOPE = 'SUBTREE'
LDAP_GROUP_MEMBERS_ATTR = 'member'
LDAP_GET_USER_ATTRIBUTES = ['cn', 'sAMAccountName', 'mail']
LDAP_GET_GROUP_ATTRIBUTES = ['cn']

CSENTRY_LDAP_GROUPS = {
    'admin': 'ICS Control System Infrastructure group',
    'create': 'ICS Employees',
}

NETWORK_DEFAULT_PREFIX = 24
# ICS Ids starting with this prefix are considered temporary and can be changed
# (waiting for a real label to be assigned)
# WARNING: This is defined here as a global settings but should not be changed!
TEMPORARY_ICS_ID = 'ZZ'
