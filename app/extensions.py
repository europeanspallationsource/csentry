# -*- coding: utf-8 -*-
"""
app.extensions
~~~~~~~~~~~~~~

This module defines the flask extensions used.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from sqlalchemy import MetaData
from flask_login import LoginManager
from flask_ldap3_login import LDAP3LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_admin import Admin
from flask_mail import Mail
from flask_jwt_extended import JWTManager
from flask_debugtoolbar import DebugToolbarExtension
from flask_redis import FlaskRedis
from flask_session import Session
from flask_caching import Cache


convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=convention)
db = SQLAlchemy(metadata=metadata, session_options={'autoflush': False})
migrate = Migrate(db=db)
login_manager = LoginManager()
ldap_manager = LDAP3LoginManager()
bootstrap = Bootstrap()
admin = Admin(template_mode='bootstrap3')
mail = Mail()
jwt = JWTManager()
toolbar = DebugToolbarExtension()
redis_store = FlaskRedis()
fsession = Session()
cache = Cache()
