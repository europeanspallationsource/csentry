$(document).ready(function() {

  function update_selectfield(field_id, data, selected_value) {
    var $field = $(field_id);
    $field.empty();
    $.map(data, function(option, index) {
      $field.append($("<option></option>").attr("value", option).text(option));
    });
    $field.val(selected_value);
  }

  function update_vlan_and_prefix() {
    // Retrieve available vlans and subnet prefixes for the selected network scope
    // and update the vlan_id and prefix select field
    var scope_id = $("#scope_id").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_vlan_and_prefix/" + scope_id,
      function(json) {
        update_selectfield("#vlan_id", json.data.vlans, json.data.selected_vlan);
        update_selectfield("#prefix", json.data.prefixes, json.data.selected_prefix);
        update_address();
      }
    );
  }

  function update_address() {
    // Retrieve available subnets for the selected network scope and prefix
    // and update the address select field
    var scope_id = $("#scope_id").val();
    var prefix = $("#prefix").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_subnets/" + scope_id + "/" + prefix,
      function(json) {
        update_selectfield("#address", json.data.subnets, json.data.selected_subnet);
        update_first_and_last_ip();
      }
    );
  }

  function update_first_and_last_ip() {
    // Retrieve IPs for the selected subnet
    // and update the first and last ip select field
    var address = $("#address").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_ips/" + address,
      function(json) {
        update_selectfield("#first_ip", json.data.ips, json.data.selected_first);
        update_selectfield("#last_ip", json.data.ips.slice().reverse(), json.data.selected_last);
      }
    );
  }

  // Populate vlan_id and prefix select field on first page load
  if( $("#scope_id").length ) {
    update_vlan_and_prefix();
  }

  // Update vlan_id and prefix select field when changing network scope
  $("#scope_id").on('change', function() {
    update_vlan_and_prefix();
  });

  // Update address select field when changing prefix
  $("#prefix").on('change', function() {
    update_address();
  });

  // Update first and last ip select field when changing address
  $("#address").on('change', function() {
    update_first_and_last_ip();
  });

  var networks_table =  $("#networks_table").DataTable({
    "ajax": function(data, callback, settings) {
      $.getJSON(
        $SCRIPT_ROOT + "/network/_retrieve_networks",
        function(json) {
          callback(json);
        });
    },
    "paging": false
  });

});
