$(document).ready(function() {

  var $copyTokenBtn = $("#copyToken");
  if( $copyTokenBtn.length ) {
    // Instantiate the clipboard only if the copyToken button exists
    var clipboard = new Clipboard($copyTokenBtn[0]);

    // show tooltip "Copied!" on success
    clipboard.on('success', function(e) {
      $copyTokenBtn.tooltip('enable');
      $copyTokenBtn.tooltip('show');
    });

    // disable tooltip when leaving button
    $copyTokenBtn.on('mouseleave', function () {
      $copyTokenBtn.tooltip('disable');
    });
  }

});
