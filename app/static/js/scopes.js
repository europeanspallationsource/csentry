$(document).ready(function() {

  var scopes_table =  $("#scopes_table").DataTable({
    "ajax": function(data, callback, settings) {
      $.getJSON(
        $SCRIPT_ROOT + "/network/_retrieve_scopes",
        function(json) {
          callback(json);
        });
    },
    "paging": false
  });

});
