$(document).ready(function() {

  function update_available_ips() {
    // Retrieve available IPs for the selected network
    // and update the IP select field
    var network_id = $("#network_id").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_available_ips/" + network_id,
      function(json) {
        var $ip = $("#ip");
        $ip.empty();
        $.map(json.data, function(option, index) {
          $ip.append($("<option></option>").attr("value", option).text(option));
        });
      }
    );
  }

  // Populate IP select field on first page load for:
  // - register new host
  // - add interface
  // Do NOT replace the IPs on edit interface page load!
  // (we have to keep the existing IP)
  if( $("#hostForm").length || $("#interfaceForm").length ) {
    update_available_ips();
  }

  // Update IP select field when changing network
  $("#network_id").on('change', function() {
    update_available_ips();
  });

  // Enable / disable item_id field depending on type
  // Item can only be assigned for physical hosts
  $("#type").on('change', function() {
    var host_type = $(this).val();
    if( host_type == "Physical" ) {
      $("#item_id").prop("disabled", false);
    } else {
      $("#item_id").val("");
      $("#item_id").prop("disabled", true);
    }
  });

  // Prefill interface name with hostname
  $("#name").keyup(function(event) {
    var hostname = $(this).val();
    $("#interface_name").val(hostname);
  });

  var hosts_table =  $("#hosts_table").DataTable({
    "ajax": function(data, callback, settings) {
      $.getJSON(
        $SCRIPT_ROOT + "/network/_retrieve_hosts",
        function(json) {
          callback(json);
        });
    },
    "pagingType": "full_numbers",
    "pageLength": 20,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
    "columnDefs": [
      {
        "targets": [0],
        "render": function(data, type, row) {
          // render funtion to create link to Item view page
          if ( data === null ) {
            return data;
          }
          var url = $SCRIPT_ROOT + "/network/hosts/view/" + data;
          return '<a href="'+ url + '">' + data + '</a>';
        }
      }
    ]
  });

});
