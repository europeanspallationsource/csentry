$(document).ready(function() {

  // scroll up to avoid having the form input
  // hidden under the navbar
  if (location.hash == "#body") {
    scrollBy(0, -100);
  }

  // Showdown extension to render links to JIRA TAG project
  var jira_tag = {
      type: 'lang',
      regex: /(TAG-\d+)/g,
      replace: '[$1](https://jira.esss.lu.se/browse/$1)'
  };
  // Register the extension
  showdown.extension('jira_tag', jira_tag);

  var converter = new showdown.Converter({
    extensions: ['jira_tag'],
    simplifiedAutoLink: true
  });

  // Live rendering of markdown comment to HTML
  $("#body").keyup(function(event) {
    var comment = $(this).val();
    $("#commentLivePreview").html(converter.makeHtml(comment));
  });

  // render existing comments to HTML
  $(".item-comment").each(function() {
    var raw = $(this).html();
    $(this).html(converter.makeHtml(raw));
  });

  // remove 'CSE:ics_id:' prefix from ICS id
  // allow to scan a label
  $("#ics_id").keyup(function(event) {
    var value = $(this).val();
    if( value.length == 17 ) {
      $(this).val(value.replace('CSE:ics_id:', ''));
    }
  });

  $("#clear").click(function() {
    // clear all select fields
    $("select").val('');
  });

  var items_table =  $("#items_table").DataTable({
    "ajax": {
      "url": $SCRIPT_ROOT + "/inventory/_retrieve_items"
    },
    "processing": true,
    "serverSide": true,
    "searchDelay": 500,
    "order": [[3, 'desc']],
    "orderMulti": false,
    "pagingType": "full_numbers",
    "pageLength": 20,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
    "columnDefs": [
      {
        "targets": [0],
        "visible": false,
        "searchable": false
      },
      {
        "targets": [6, 7, 8, 9],
        "orderable": false
      },
      {
        "targets": [1, 10],
        "render": function(data, type, row) {
          // render funtion to create link to Item view page
          if ( data === null ) {
            return data;
          }
          var url = $SCRIPT_ROOT + "/inventory/items/view/" + data;
          return '<a href="'+ url + '">' + data + '</a>';
        }
      }
    ]
  });

});
