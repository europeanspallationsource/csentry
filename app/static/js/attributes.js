$(document).ready(function() {

  var attributes_table =  $("#attributes_table").DataTable({
    "ajax": function(data, callback, settings) {
      var kind = $('li a.nav-link.active').text();
      $.getJSON(
        $SCRIPT_ROOT + "/inventory/_retrieve_attributes_name/" + kind,
        function(json) {
          callback(json);
        });
    },
    "paging": false
  });

});
