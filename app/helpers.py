# -*- coding: utf-8 -*-
"""
app.helpers
~~~~~~~~~~~

This module implements helpers functions for the models.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import sqlalchemy as sa
from flask_wtf import FlaskForm
from .extensions import db
from . import models


class CSEntryForm(FlaskForm):

    def __init__(self, formdata=None, obj=None, **kwargs):
        # Store obj for Unique validator to check if the unique object
        # is identical to the one being edited
        self._obj = obj
        # formdata is often given as first argument (not keyword argument)
        # It's initialized as a singleton in flask-wtf so we should only pass
        # it if not None
        if formdata is None:
            super().__init__(obj=obj, **kwargs)
        else:
            super().__init__(formdata=formdata, obj=obj, **kwargs)


def associate_mac_to_interface(address, interface):
    """Associate the given address to an interface

    The Mac is retrieved if it exists or created otherwise

    :param address: Mac address string
    :param interface: Interface instance
    """
    if not address:
        return
    try:
        mac = models.Mac.query.filter_by(address=address).one()
    except sa.orm.exc.NoResultFound:
        mac = models.Mac(address=address)
        db.session.add(mac)
    mac.interfaces.append(interface)
