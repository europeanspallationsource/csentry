# -*- coding: utf-8 -*-
"""
app.utils
~~~~~~~~~

This module implements utility functions.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import base64
import datetime
import io
import sqlalchemy as sa
import dateutil.parser
from flask.globals import _app_ctx_stack, _request_ctx_stack
from flask_login import current_user
from flask_jwt_extended import get_current_user


def fetch_current_user_id():
    """Retrieve the user_id from flask_jwt_extended (API) or flask_login (web UI)"""
    # Return None if we are outside of request context.
    if _app_ctx_stack.top is None or _request_ctx_stack.top is None:
        return None
    # Try to get the user from both flask_jwt_extended and flask_login
    user = get_current_user() or current_user
    try:
        return user.id
    except AttributeError:
        return None


class CSEntryError(Exception):
    """CSEntryError class

    Exception used to pass useful information to the client side (API or AJAX)
    """
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

    def __str__(self):
        return str(self.to_dict())


def image_to_base64(img, format='PNG'):
    """Convert a Pillow image to a base64 string

    :param img: Pillow image
    :param format: format of the image to use
    :returns str: image as base64 string
    """
    buf = io.BytesIO()
    img.save(buf, format=format)
    return base64.b64encode(buf.getvalue()).decode('ascii')


def format_field(field):
    """Format the given field to a string or None"""
    if field is None:
        return None
    if isinstance(field, datetime.datetime):
        return field.strftime('%Y-%m-%d %H:%M')
    return str(field)


def convert_to_model(item, model, filter='name'):
    """Convert item to an instance of model

    Allow to convert a string to an instance of model
    Raise an exception if the given name is not found

    :returns: an instance of model
    """
    if item is None:
        return None
    if not isinstance(item, model):
        kwarg = {filter: item}
        instance = model.query.filter_by(**kwarg).first()
        if instance is None:
            raise CSEntryError(f'{item} is not a valid {model.__name__.lower()}')
        return instance
    return item


def attribute_to_string(value):
    """Return the attribute as a string

    If the attribute is defined in the schema as multi valued
    then the attribute value is returned as a list
    See http://ldap3.readthedocs.io/tutorial_searches.html#entries-retrieval

    This function returns the first item of the list if it's a list

    :param value: string or list
    :returns: string
    """
    if isinstance(value, list):
        return value[0]
    else:
        return value


def get_choices(iterable, allow_blank=False, allow_null=False):
    """Return a list of (value, label)"""
    choices = []
    if allow_blank:
        choices = [('', '')]
    if allow_null:
        choices.append(('null', 'not set'))
    choices.extend([(val, val) for val in iterable])
    return choices


def get_model_choices(model, allow_none=False, attr='name', query=None):
    """Return a list of (value, label)"""
    choices = []
    if allow_none:
        choices = [(None, '')]
    if query is None:
        query = model.query.order_by(getattr(model, attr))
    choices.extend([(str(instance.id), getattr(instance, attr)) for instance in query.all()])
    return choices


def get_query(query, **kwargs):
    """Retrieve the query from the arguments

    :param query: sqlalchemy base query
    :param kwargs: kwargs from a request
    :returns: query filtered by the arguments
    """
    if kwargs:
        try:
            query = query.filter_by(**kwargs)
        except (sa.exc.InvalidRequestError, AttributeError) as e:
            raise CSEntryError('Invalid query arguments', status_code=422)
    return query


def lowercase_field(value):
    """Filter to force form value to lowercase"""
    try:
        return value.lower()
    except AttributeError:
        return value


# coerce function to use with SelectField that can accept a None value
# wtforms always coerce to string by default
# Values returned from the form are usually strings but if a field is disabled
# None is returned
# To pass wtforms validation, the value returned must be part of choices
def coerce_to_str_or_none(value):
    """Convert '', None and 'None' to None"""
    if not value or value == 'None':
        return None
    return str(value)


def parse_to_utc(string):
    """Convert a string to a datetime object with no timezone"""
    d = dateutil.parser.parse(string)
    if d.tzinfo is None:
        # Assume this is UTC
        return d
    # Convert to UTC and remove timezone
    d = d.astimezone(datetime.timezone.utc)
    return d.replace(tzinfo=None)
