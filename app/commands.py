# -*- coding: utf-8 -*-
"""
app.commands
~~~~~~~~~~~~

This module defines extra flask commands.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import ldap3
import sqlalchemy as sa
from flask import current_app
from .extensions import db, ldap_manager
from .defaults import defaults
from .models import User
from . import utils, tokens


def sync_user(connection, user):
    """Synchronize the user from the database with information from the LDAP server"""
    search_attr = current_app.config.get('LDAP_USER_LOGIN_ATTR')
    object_filter = current_app.config.get('LDAP_USER_OBJECT_FILTER')
    search_filter = f'(&{object_filter}({search_attr}={user.username}))'
    connection.search(
        search_base=ldap_manager.full_user_search_dn,
        search_filter=search_filter,
        search_scope=getattr(
            ldap3, current_app.config.get('LDAP_USER_SEARCH_SCOPE')),
        attributes=current_app.config.get('LDAP_GET_USER_ATTRIBUTES')
    )
    if len(connection.response) == 1:
        ldap_user = connection.response[0]
        attributes = ldap_user['attributes']
        user.display_name = utils.attribute_to_string(attributes['cn'])
        user.email = utils.attribute_to_string(attributes['mail'])
        groups = ldap_manager.get_user_groups(dn=ldap_user['dn'], _connection=connection)
        user.groups = sorted([utils.attribute_to_string(group['cn']) for group in groups])
        current_app.logger.info(f'{user} updated')
    else:
        # Clear user's groups
        user.groups = []
        # Revoke all user's tokens
        for token in user.tokens:
            db.session.delete(token)
        current_app.logger.info(f'{user} disabled')
    return user


def sync_users():
    """Synchronize all users from the database with information the LDAP server"""
    current_app.logger.info('Synchronize database with information from the LDAP server')
    try:
        connection = ldap_manager.connection
    except ldap3.core.exceptions.LDAPException as e:
        current_app.logger.warning(f'Failed to connect to the LDAP server: {e}')
        return
    for user in User.query.all():
        sync_user(connection, user)
    db.session.commit()


def register_cli(app):
    @app.cli.command()
    def initdb():
        """Create the database tables and initialize them with default values"""
        db.engine.execute('CREATE EXTENSION IF NOT EXISTS citext')
        db.create_all()
        for instance in defaults:
            db.session.add(instance)
            try:
                db.session.commit()
            except sa.exc.IntegrityError as e:
                db.session.rollback()
                app.logger.debug(f'{instance} already exists')

    @app.cli.command()
    def syncusers():
        """Synchronize all users from the database with information the LDAP server"""
        sync_users()

    @app.cli.command()
    def delete_expired_tokens():
        """Prune database from expired tokens"""
        tokens.prune_database()

    @app.cli.command()
    def maintenance():
        """Run maintenance commands"""
        sync_users()
        tokens.prune_database()
