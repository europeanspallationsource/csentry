# -*- coding: utf-8 -*-
"""
app.api.inventory
~~~~~~~~~~~~~~~~~

This module implements the inventory API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint, jsonify, request, current_app
from flask_jwt_extended import jwt_required
from .. import utils, models
from ..decorators import jwt_groups_accepted
from .utils import commit, create_generic_model, get_generic_model

bp = Blueprint('inventory_api', __name__)


def get_item_by_id_or_ics_id(id_):
    """Retrieve item by id or ICS id"""
    try:
        item_id = int(id_)
    except ValueError:
        # Assume id_ is an ics_id
        item = models.Item.query.filter_by(ics_id=id_).first()
    else:
        item = models.Item.query.get(item_id)
    if item is None:
        raise utils.CSEntryError(f"Item id '{id_}' not found", status_code=404)
    return item


@bp.route('/items')
@jwt_required
def get_items():
    return get_generic_model(models.Item,
                             order_by=models.Item.created_at)


@bp.route('/items/<id_>')
@jwt_required
def get_item(id_):
    """Retrieve item by id or ICS id"""
    item = get_item_by_id_or_ics_id(id_)
    return jsonify(item.to_dict())


@bp.route('/items', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_item():
    """Register a new item"""
    # People should assign an ICS id to a serial number when creating
    # an item so ics_id should also be a mandatory field.
    # But there are existing items (in confluence and JIRA) that we want to
    # import and associate after they have been created.
    # In that case a temporary id is automatically assigned.
    return create_generic_model(models.Item, mandatory_fields=('serial_number',))


@bp.route('/items/<id_>', methods=['PATCH'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def patch_item(id_):
    """Patch an existing item

    id_ can be the primary key or the ics_id field
    Fields allowed to update are:
        - ics_id ONLY if current is temporary (422 returned otherwise)
        - manufacturer
        - model
        - location
        - status
        - parent

    422 is returned if other fields are given.
    """
    data = request.get_json()
    if data is None:
        raise utils.CSEntryError('Body should be a JSON object')
    if not data:
        raise utils.CSEntryError('At least one field is required', status_code=422)
    for key in data.keys():
        if key not in ('ics_id', 'manufacturer', 'model',
                       'location', 'status', 'parent'):
            raise utils.CSEntryError(f"Invalid field '{key}'", status_code=422)
    item = get_item_by_id_or_ics_id(id_)
    # Only allow to set ICS id if the current id is a temporary one
    if item.ics_id.startswith(current_app.config['TEMPORARY_ICS_ID']):
        item.ics_id = data.get('ics_id', item.ics_id)
    elif 'ics_id' in data:
        raise utils.CSEntryError("'ics_id' can't be changed", status_code=422)
    item.manufacturer = utils.convert_to_model(data.get('manufacturer', item.manufacturer), models.Manufacturer)
    item.model = utils.convert_to_model(data.get('model', item.model), models.Model)
    item.location = utils.convert_to_model(data.get('location', item.location), models.Location)
    item.status = utils.convert_to_model(data.get('status', item.status), models.Status)
    parent_ics_id = data.get('parent')
    if parent_ics_id is not None:
        parent = models.Item.query.filter_by(ics_id=parent_ics_id).first()
        if parent is not None:
            item.parent_id = parent.id
            # Update location and status with those from parent
            item.location = parent.location
            item.status = parent.status
    # Update all children status and location
    for child in item.children:
        child.location = item.location
        child.status = item.status
    commit()
    return jsonify(item.to_dict())


@bp.route('/items/<id_>/comments')
@jwt_required
def get_item_comments(id_):
    item = get_item_by_id_or_ics_id(id_)
    return jsonify([comment.to_dict() for comment in item.comments])


@bp.route('/items/<id_>/comments', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_item_comment(id_):
    item = get_item_by_id_or_ics_id(id_)
    return create_generic_model(models.ItemComment,
                                mandatory_fields=('body',),
                                item_id=item.id)


@bp.route('/actions')
@jwt_required
def get_actions():
    return get_generic_model(models.Action)


@bp.route('/manufacturers')
@jwt_required
def get_manufacturers():
    return get_generic_model(models.Manufacturer)


@bp.route('/manufacturers', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_manufacturer():
    return create_generic_model(models.Manufacturer)


@bp.route('/models')
@jwt_required
def get_models():
    return get_generic_model(models.Model)


@bp.route('/models', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_model():
    return create_generic_model(models.Model)


@bp.route('/locations')
@jwt_required
def get_locations():
    return get_generic_model(models.Location)


@bp.route('/locations', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_locations():
    return create_generic_model(models.Location)


@bp.route('/statuses')
@jwt_required
def get_status():
    return get_generic_model(models.Status)


@bp.route('/statuses', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_status():
    return create_generic_model(models.Status)
