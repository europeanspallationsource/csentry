# -*- coding: utf-8 -*-
"""
app.api.network
~~~~~~~~~~~~~~~

This module implements the network API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint
from flask_jwt_extended import jwt_required
from .. import models
from ..decorators import jwt_groups_accepted
from .utils import get_generic_model, create_generic_model

bp = Blueprint('network_api', __name__)


@bp.route('/scopes')
@jwt_required
def get_scopes():
    return get_generic_model(models.NetworkScope,
                             order_by=models.NetworkScope.name)


@bp.route('/scopes', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin')
def create_scope():
    """Create a new network scope"""
    return create_generic_model(models.NetworkScope, mandatory_fields=(
        'name', 'first_vlan', 'last_vlan', 'supernet'))


@bp.route('/networks')
@jwt_required
def get_networks():
    return get_generic_model(models.Network,
                             order_by=models.Network.address)


@bp.route('/networks', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin')
def create_network():
    """Create a new network"""
    return create_generic_model(models.Network, mandatory_fields=(
        'vlan_name', 'vlan_id', 'address', 'first_ip', 'last_ip', 'scope'))


@bp.route('/interfaces')
@jwt_required
def get_interfaces():
    return get_generic_model(models.Interface,
                             order_by=models.Interface.ip)


@bp.route('/interfaces', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_interface():
    """Create a new interface"""
    # The validate_interfaces method from the Network class is called when
    # setting interface.network. This is why we don't pass network_id here
    # but network (as vlan_name string)
    return create_generic_model(models.Interface, mandatory_fields=('network', 'ip', 'name'))


@bp.route('/hosts')
@jwt_required
def get_hosts():
    return get_generic_model(models.Host,
                             order_by=models.Host.name)


@bp.route('/hosts', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_host():
    """Create a new host"""
    return create_generic_model(models.Host, mandatory_fields=('name',))


@bp.route('/macs')
@jwt_required
def get_macs():
    return get_generic_model(models.Mac,
                             order_by=models.Mac.address)


@bp.route('/macs', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin', 'create')
def create_macs():
    return create_generic_model(models.Mac,
                                mandatory_fields=('address',))
