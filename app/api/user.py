# -*- coding: utf-8 -*-
"""
app.api.user
~~~~~~~~~~~~

This module implements the user API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import current_app, Blueprint, jsonify, request
from flask_ldap3_login import AuthenticationResponseStatus
from flask_jwt_extended import jwt_required
from ..extensions import ldap_manager
from ..decorators import jwt_groups_accepted
from .. import utils, tokens, models
from .utils import get_generic_model, create_generic_model

bp = Blueprint('user_api', __name__)


@bp.route('/users')
@jwt_required
def get_users():
    return get_generic_model(models.User,
                             order_by=models.User.username)


@bp.route('/users', methods=['POST'])
@jwt_required
@jwt_groups_accepted('admin')
def create_user():
    """Create a new user"""
    return create_generic_model(models.User, mandatory_fields=(
        'username', 'display_name', 'email'))


@bp.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    if data is None:
        raise utils.CSEntryError('Body should be a JSON object')
    try:
        username = data['username']
        password = data['password']
    except KeyError:
        raise utils.CSEntryError('Missing mandatory field (username or password)', status_code=422)
    response = ldap_manager.authenticate(username, password)
    if response.status == AuthenticationResponseStatus.success:
        current_app.logger.debug(f'{username} successfully logged in')
        user = ldap_manager._save_user(
            response.user_dn,
            response.user_id,
            response.user_info,
            response.user_groups)
        payload = {'access_token': tokens.generate_access_token(identity=user.id)}
        return jsonify(payload), 200
    raise utils.CSEntryError('Invalid credentials', status_code=401)
