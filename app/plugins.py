# -*- coding: utf-8 -*-
"""
app.plugins
~~~~~~~~~~~

This module implements SQLAlchemy-Continuum plugins.

FlaskUserPlugin offers way of integrating Flask framework with
SQLAlchemy-Continuum. FlaskUser-Plugin adds a `user_id` column for Transaction model.

This plugin is based on the official FlaskPlugin with the following modifications:
    - no remote_addr column
    - the user_id is taken from flask_jwt_extended (API) or flask_login (web UI)

The `user_id` column is automatically populated when the transaction object is created.

:copyright: original (c) 2012, Konsta Vesterinen
:copyright: modified (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""

from sqlalchemy_continuum.plugins import Plugin
from . import utils


class FlaskUserPlugin(Plugin):

    def __init__(self, current_user_id_factory=None):
        self.current_user_id_factory = (
            utils.fetch_current_user_id if current_user_id_factory is None
            else current_user_id_factory
        )

    def transaction_args(self, uow, session):
        return {
            'user_id': self.current_user_id_factory(),
        }
