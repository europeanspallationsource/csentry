# -*- coding: utf-8 -*-
"""
tests.functional.common
~~~~~~~~~~~~~~~~~~~~~~~

Define common functions and variables used in the tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from sqlalchemy import orm

# create the global scope_session for the tests
# See http://factoryboy.readthedocs.io/en/latest/orms.html#managing-sessions
Session = orm.scoped_session(orm.sessionmaker())
