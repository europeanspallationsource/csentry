# -*- coding: utf-8 -*-
"""
tests.functional.conftest
~~~~~~~~~~~~~~~~~~~~~~~~~

Pytest fixtures common to all functional tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import pytest
import sqlalchemy as sa
from pytest_factoryboy import register
from flask_ldap3_login import AuthenticationResponse, AuthenticationResponseStatus
from app.factory import create_app
from app.extensions import db as _db
from . import common, factories

register(factories.UserFactory)
register(factories.ActionFactory)
register(factories.ManufacturerFactory)
register(factories.ModelFactory)
register(factories.LocationFactory)
register(factories.StatusFactory)
register(factories.ItemFactory)
register(factories.NetworkScopeFactory)
register(factories.NetworkFactory)
register(factories.InterfaceFactory)
register(factories.HostFactory)
register(factories.MacFactory)


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    config = {
        'TESTING': True,
        'WTF_CSRF_ENABLED': False,
        'SQLALCHEMY_DATABASE_URI': 'postgresql://ics:icspwd@postgres/csentry_db_test',
        'CSENTRY_LDAP_GROUPS': {
            'admin': 'CSEntry Admin',
            'create': 'CSEntry User',
        }
    }
    app = create_app(config=config)
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture
def client(request, app):
    return app.test_client()


@pytest.fixture(scope='session')
def db(app, request):
    """Session-wide test database."""
    def teardown():
        _db.session.remove()
        _db.drop_all()

    _db.app = app
    _db.engine.execute('CREATE EXTENSION IF NOT EXISTS citext')
    _db.create_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(autouse=True)
def session(db, request):
    """Creates a new database session for every test.

    Rollback any transaction to always leave the database clean
    """
    connection = db.engine.connect()
    transaction = connection.begin()
    session = common.Session
    session.configure(bind=connection, autoflush=False)
    session.begin_nested()

    # session is actually a scoped_session
    # for the `after_transaction_end` event, we need a session instance to
    # listen for, hence the `session()` call
    @sa.event.listens_for(session(), 'after_transaction_end')
    def resetart_savepoint(sess, trans):
        if trans.nested and not trans._parent.nested:
            session.expire_all()
            session.begin_nested()

    db.session = session

    yield session

    session.remove()
    transaction.rollback()
    connection.close()


@pytest.fixture(autouse=True)
def no_ldap_connection(monkeypatch):
    """Make sure we don't make any connection to the LDAP server"""
    monkeypatch.delattr('flask_ldap3_login.LDAP3LoginManager._make_connection')


@pytest.fixture(autouse=True)
def patch_ldap_authenticate(monkeypatch):

    def authenticate(self, username, password):
        response = AuthenticationResponse()
        response.user_id = username
        response.user_dn = f'cn={username},dc=esss,dc=lu,dc=se'
        if username == 'admin' and password == 'adminpasswd':
            response.status = AuthenticationResponseStatus.success
            response.user_info = {'cn': 'Admin User', 'mail': 'admin@example.com'}
            response.user_groups = [{'cn': 'CSEntry Admin'}]
        elif username == 'user_rw' and password == 'userrw':
            response.status = AuthenticationResponseStatus.success
            response.user_info = {'cn': 'User RW', 'mail': 'user_rw@example.com'}
            response.user_groups = [{'cn': 'CSEntry User'}]
        elif username == 'user_ro' and password == 'userro':
            response.status = AuthenticationResponseStatus.success
            response.user_info = {'cn': 'User RO', 'mail': 'user_ro@example.com'}
            response.user_groups = [{'cn': 'ESS Employees'}]
        else:
            response.status = AuthenticationResponseStatus.fail
        return response

    monkeypatch.setattr('flask_ldap3_login.LDAP3LoginManager.authenticate', authenticate)
