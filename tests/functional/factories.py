# -*- coding: utf-8 -*-
"""
tests.functional.factories
~~~~~~~~~~~~~~~~~~~~~~~~~~

This module defines models factories.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import ipaddress
import factory
from faker import Factory as FakerFactory
from app import models
from . import common


faker = FakerFactory.create()


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.User
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    username = factory.Sequence(lambda n: f'username{n}')
    display_name = factory.LazyAttribute(lambda o: f'long {o.username}')


class ActionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Action
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'action{n}')


class ManufacturerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Manufacturer
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'manufacturer{n}')


class ModelFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Model
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'model{n}')


class LocationFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Location
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'location{n}')


class StatusFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Status
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'status{n}')


class ItemFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Item
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    ics_id = factory.Sequence(lambda n: f'AAA{n:03}')
    serial_number = factory.Faker('isbn10')
    manufacturer = factory.SubFactory(ManufacturerFactory)
    model = factory.SubFactory(ModelFactory)
    location = factory.SubFactory(LocationFactory)
    status = factory.SubFactory(StatusFactory)
    user = factory.SubFactory(UserFactory)


class NetworkScopeFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.NetworkScope
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'scope{n}')
    first_vlan = factory.Sequence(lambda n: 1600 + 10 * n)
    last_vlan = factory.Sequence(lambda n: 1609 + 10 * n)
    supernet = factory.Faker('ipv4', network=True)
    user = factory.SubFactory(UserFactory)


class NetworkFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Network
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    vlan_name = factory.Sequence(lambda n: f'vlan{n}')
    vlan_id = factory.Sequence(lambda n: 1600 + n)
    address = factory.Faker('ipv4', network=True)
    scope = factory.SubFactory(NetworkScopeFactory)
    user = factory.SubFactory(UserFactory)

    @factory.lazy_attribute
    def first_ip(self):
        net = ipaddress.ip_network(self.address)
        hosts = list(net.hosts())
        return str(hosts[4])

    @factory.lazy_attribute
    def last_ip(self):
        net = ipaddress.ip_network(self.address)
        hosts = list(net.hosts())
        return str(hosts[-5])


class InterfaceFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Interface
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'host{n}')
    network = factory.SubFactory(NetworkFactory)
    ip = factory.LazyAttributeSequence(lambda o, n: str(ipaddress.ip_address(o.network.first_ip) + n))
    user = factory.SubFactory(UserFactory)


class HostFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Host
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    name = factory.Sequence(lambda n: f'host{n}')
    user = factory.SubFactory(UserFactory)


class MacFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Mac
        sqlalchemy_session = common.Session
        sqlalchemy_session_persistence = 'commit'

    address = factory.Faker('mac_address')
