# -*- coding: utf-8 -*-
"""
tests.functional.test_models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module defines models tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import ipaddress
import pytest
from wtforms import ValidationError


def test_user_groups(user_factory):
    user = user_factory()
    assert user.groups == []
    groups = ['foo', 'Another group']
    user = user_factory(groups=groups)
    assert user.groups == groups


def test_network_ip_properties(network_factory):
    # Create some networks
    network1 = network_factory(address='172.16.1.0/24', first_ip='172.16.1.10', last_ip='172.16.1.250')
    network2 = network_factory(address='172.16.20.0/26', first_ip='172.16.20.11', last_ip='172.16.20.14')

    assert network1.network_ip == ipaddress.ip_network('172.16.1.0/24')
    assert network1.first == ipaddress.ip_address('172.16.1.10')
    assert network1.last == ipaddress.ip_address('172.16.1.250')
    assert len(network1.ip_range()) == 241
    assert network1.ip_range() == [ipaddress.ip_address(f'172.16.1.{i}') for i in range(10, 251)]
    assert network1.ip_range() == network1.available_ips()
    assert network1.used_ips() == []

    assert network2.network_ip == ipaddress.ip_network('172.16.20.0/26')
    assert network2.first == ipaddress.ip_address('172.16.20.11')
    assert network2.last == ipaddress.ip_address('172.16.20.14')
    assert len(network2.ip_range()) == 4
    assert network2.ip_range() == [ipaddress.ip_address(f'172.16.20.{i}') for i in range(11, 15)]
    assert network2.ip_range() == network2.available_ips()
    assert network2.used_ips() == []


def test_network_available_and_used_ips(network_factory, interface_factory):
    # Create some networks and interfaces
    network1 = network_factory(address='172.16.1.0/24', first_ip='172.16.1.10', last_ip='172.16.1.250')
    network2 = network_factory(address='172.16.20.0/26', first_ip='172.16.20.11', last_ip='172.16.20.14')
    for i in range(10, 20):
        interface_factory(network=network1, ip=f'172.16.1.{i}')
    interface_factory(network=network2, ip='172.16.20.13')
    # Check available and used IPs
    assert network1.used_ips() == [ipaddress.ip_address(f'172.16.1.{i}') for i in range(10, 20)]
    assert network1.available_ips() == [ipaddress.ip_address(f'172.16.1.{i}') for i in range(20, 251)]
    assert network2.used_ips() == [ipaddress.ip_address('172.16.20.13')]
    assert network2.available_ips() == [ipaddress.ip_address('172.16.20.11'),
                                        ipaddress.ip_address('172.16.20.12'),
                                        ipaddress.ip_address('172.16.20.14')]

    # Add more interfaces
    interface_factory(network=network2, ip='172.16.20.11')
    interface_factory(network=network2, ip='172.16.20.14')
    assert len(network2.used_ips()) == 3
    assert network2.used_ips() == [ipaddress.ip_address('172.16.20.11'),
                                   ipaddress.ip_address('172.16.20.13'),
                                   ipaddress.ip_address('172.16.20.14')]
    assert network2.available_ips() == [ipaddress.ip_address('172.16.20.12')]

    # Add last available IP
    interface_factory(network=network2, ip='172.16.20.12')
    assert network2.used_ips() == [ipaddress.ip_address(f'172.16.20.{i}') for i in range(11, 15)]
    assert list(network2.available_ips()) == []


def test_mac_address_validation(mac_factory):
    mac = mac_factory(address='F4:A7:39:15:DA:01')
    assert mac.address == 'f4:a7:39:15:da:01'
    mac = mac_factory(address='F4-A7-39-15-DA-02')
    assert mac.address == 'f4:a7:39:15:da:02'
    mac = mac_factory(address='F4A73915DA06')
    assert mac.address == 'f4:a7:39:15:da:06'
    with pytest.raises(ValidationError) as excinfo:
        mac = mac_factory(address='F4A73915DA')
    assert "'F4A73915DA' does not appear to be a MAC address" in str(excinfo.value)
