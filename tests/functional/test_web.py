# -*- coding: utf-8 -*-
"""
tests.functional.test_web
~~~~~~~~~~~~~~~~~~~~~~~~~

This module defines basic web tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import json
import pytest


def get(client, url):
    response = client.get(url)
    if response.headers['Content-Type'] == 'application/json':
        response.json = json.loads(response.data)
    return response


def login(client, username, password):
    data = {
        'username': username,
        'password': password
    }
    return client.post('/user/login', data=data, follow_redirects=True)


def logout(client):
    return client.get('/user/logout', follow_redirects=True)


@pytest.fixture
def logged_client(client):
    login(client, 'user_ro', 'userro')
    return client


def test_login_logout(client):
    response = login(client, 'unknown', 'invalid')
    assert b'<title>Login - CSEntry</title>' in response.data
    response = login(client, 'user_rw', 'invalid')
    assert b'<title>Login - CSEntry</title>' in response.data
    response = login(client, 'user_rw', 'userrw')
    assert b'Welcome to CSEntry!' in response.data
    assert b'User RW' in response.data
    response = logout(client)
    assert b'<title>Login - CSEntry</title>' in response.data


def test_index(logged_client):
    response = logged_client.get('/')
    assert b'Welcome to CSEntry!' in response.data
    assert b'User RO' in response.data


@pytest.mark.parametrize('url', [
    '/',
    '/inventory/items',
    '/inventory/_retrieve_items',
    '/network/networks',
])
def test_protected_url(url, client):
    response = client.get(url)
    assert response.status_code == 302
    assert '/user/login' in response.headers['Location']
    login(client, 'user_ro', 'userro')
    response = client.get(url)
    assert response.status_code == 200


def test_retrieve_items(logged_client, item_factory):
    response = get(logged_client, '/inventory/_retrieve_items')
    assert response.json['data'] == []
    serial_numbers = ('12345', '45678')
    for sn in serial_numbers:
        item_factory(serial_number=sn)
    response = get(logged_client, '/inventory/_retrieve_items')
    items = response.json['data']
    assert set(serial_numbers) == set(item[4] for item in items)
    assert len(items[0]) == 11
